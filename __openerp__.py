# -*- coding: utf-8 -*-
{
    'name': "Zold Quiz App",

    'summary': """
            Quiz app for real time feedback...
        """,

    'description': """

    """,

    'author': "Aman Gautam",
    'website': "http://www.zold.co",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'REST APIs',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'templates.xml',
        'session/view.xml',
        'session/menu.xml',
        'question/view.xml',
        'question/menu.xml',
        'question/workflow.xml',
        'option/view.xml',
        'option/menu.xml',
        'response/menu.xml',
        'response/view.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}