from openerp import models, fields


class ZoldQuizResponse(models.Model):

    _name = 'zold_quiz.response'

    name = fields.Char() # Not sure why we should even use it.
    question_id = fields.Many2one('zold_quiz.question')
    options_selected_id = fields.Many2many('zold_quiz.option', domain=['question','=',1])
    attempt = fields.Integer(default=1)
    response = fields.Text()