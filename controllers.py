# -*- coding: utf-8 -*-
from openerp import http

class ZoldRest(http.Controller):

    @http.route('/api/', auth='public')
    def index(self, **kw):
        return "Hello, world"

#     @http.route('/zold_rest/zold_rest/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('zold_rest.listing', {
#             'root': '/zold_rest/zold_rest',
#             'objects': http.request.env['zold_rest.zold_rest'].search([]),
#         })

#     @http.route('/zold_rest/zold_rest/objects/<model("zold_rest.zold_rest"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('zold_rest.object', {
#             'object': obj
#         })