from openerp import models, fields


class ZoldQuestionOptions(models.Model):
    _name = 'zold_quiz.option'

    name = fields.Char('Option')
    question = fields.Many2one('zold_quiz.question')
    is_answer = fields.Boolean(string='Right answer?')
    code = fields.Char(length=1) # a,b,c,d,e...