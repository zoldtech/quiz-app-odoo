from openerp import models, fields, api


class ZoldQuestion(models.Model):
    _name = 'zold_quiz.question'

    def default_user_id(self):
        return self.env.user

    name = fields.Char('Question', required=True)  # the question statement
    user_id = fields.Many2one('res.users', default=default_user_id, readonly=True)
    option_ids = fields.One2many('zold_quiz.option', 'question')

    # It is possible to give students a chance to take the quiz again.
    no_attempts = fields.Integer()
    state = fields.Selection \
        ([
             ('draft', 'Draft'),
             ('start', 'Start'),
             ('stop', 'Stop'),
             ('show_result', 'Show Result'),
             ('end', 'End')
         ], default='draft')

    @api.one
    def draft(self, *args, **kwargs):
        self.state = 'draft'

    @api.one
    def start(self, *args, **kwargs):
        self.state = 'start'

    @api.one
    def stop(self, *args, **kwargs):
        self.state = 'stop'

    @api.one
    def show_result(self, *args, **kwargs):
        self.state = 'show_result'

    @api.one
    def end(self, *args, **kwargs):
        self.state = 'end'

    @api.one
    def restart(self, *args, **kwargs):
        self.state = 'start'
        self.no_attempts += 1