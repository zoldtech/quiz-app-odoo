from openerp import models, fields, api
from uuid import uuid4


class ZoldQuiz(models.Model):
    _name = 'zold_quiz.session'

    def default_name(self):
        return uuid4()

    def default_user(self):
        return self.env.user

    name = fields.Char(string='Title', default=default_name, help='Name of the quiz', required=True,
                       _sql_constraints=[('name_unique', 'unique(name)', 'name already exists!')])
    user_id = fields.Many2one('res.users', default=default_user)
    class_starts = fields.Datetime()
    class_ends = fields.Datetime()
    is_public = fields.Boolean() # to make the session public and not limited to only the students of the class..
    course_id = fields.Many2one('op.course')
    batch_id = fields.Many2one('op.batch')
    standard_id = fields.Many2one('op.standard')
    division_id = fields.Many2one('op.division')

